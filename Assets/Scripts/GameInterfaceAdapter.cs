﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class GameInterfaceAdapter : MonoBehaviour {

	public GameObject partidaGO;
	public GameObject playerGO;

	Partida partida;
	Player player;

	public Text textRound;

	//player 01
	public Text playerName;
	public Text playerScore;
	public Text curralName;
	public Text curralGrass;

	void Start () {
		player = playerGO.GetComponent<Player> ();
		partida 	 = partidaGO.GetComponent<Partida> ();

		playerName.text = player.name;
		curralName.text = player.nick;
	}

	void Update () {
		if(partida.round < 10){
			textRound.text = "Rodada 0" + partida.round;
		}else{
			textRound.text = "Rodada " + partida.round;
		}

		playerScore.text = scorePlayer ();
		curralGrass.text = countSheeps ();
	}

	public string scorePlayer(){
		if (player.score < 10) {
			return "0000" + player.score;
		} else if (player.score < 100) {
			return "000" + player.score;
		} else if (player.score < 1000) {
			return "00" + player.score;
		} else{
			return "0" + player.score;
		}
	}

	public string countSheeps(){
		if (player.sheeps.Count < 10) {
			return "x0" + (player.sheeps.Count);		
		} else {
			return "x" + (player.sheeps.Count );
		}
	}

}