﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class DebugsOnTheTable : MonoBehaviour {

	public GameObject partidaGO;

	Partida p;

	public Text text1;
	public Text text2;
	public Text text3;
	public Text text4;

	// Use this for initialization
	void Start () {
	
		p = partidaGO.GetComponent<Partida>();
	}
	
	// Update is called once per frame
	void Update () {
		text1.text = "Grass: " + p.grass;
		text2.text = "time: "+ p.timecount;
		text3.text = "Rodada: "+ p.round;
		text4.text = "Partida sheeps: "+ p.sheeps.Count;
	}
}
