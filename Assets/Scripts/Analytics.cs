﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Text;
using System;
using UnityEngine.UI;

public class Analytics : MonoBehaviour {

	public GameObject playerGO;
	public static Player player;
	public static String logRodadaAtual;
	public Text lugar;
	public static String dataAtual = "[" + DateTime.Now.ToString ("HH:mm:ss") + "] ";
	//public static String path = "\\sdcard\\victtao.txt";
	public static string path;// = Application.persistentDataPath + "\\"+ Settings.playerName + "_" + DateTime.Now.ToString ("yyyy-MM-dd HH-mm-ss") + ".txt";

	// Use this for initialization
	void Start () {
		Debug.Log (dataAtual);
		logRodadaAtual = "";
		player = playerGO.GetComponent<Player>();

		string driveLetter = Environment.CurrentDirectory;

		path =  driveLetter.Replace(@"\", "/");
		path += "\\"+ Settings.playerName + "_" +	DateTime.Now.ToString ("yyyy-MM-dd HH-mm-ss") + ".txt";
		path = Application.persistentDataPath + "\\"+ Settings.playerName + "_" +	DateTime.Now.ToString ("yyyy-MM-dd HH-mm-ss") + ".txt";
		// This text is added only once to the file. 
		if (!File.Exists(path))
		{
			string createText = "["+ DateTime.Now.ToString ("yyyy-MM-dd HH:mm:ss") + "]" + " - Criado arquivo." + Environment.NewLine;
			File.WriteAllText(path, createText);
		}
		
		// Open the file to read from. 
		string readText = File.ReadAllText(path);
		Console.WriteLine(readText);
	}

	public static void addToLog(String msg){
		string appendText = msg + Environment.NewLine;
		File.AppendAllText(path, appendText);
	}
	
	// Update is called once per frame
	void Update () {
		dataAtual = "[" + DateTime.Now.ToString ("HH:mm:ss") + "] ";
		lugar.text = path;
	}

	void writeHighScore() {
		string path = "\\sdcard\\colorbounce\\highscore.txt";
		int highscore = 0;
		// This text is added only once to the file. 
		
		if (!File.Exists(path)) 
		{
			System.IO.Directory.CreateDirectory("\\sdcard\\colorbounce\\");
			// Create a file to write to. 
			using (StreamWriter sw = File.CreateText(path)) 
			{
				sw.WriteLine("IMPRIMIU ALGO");
			}   
		}
		// Open the file to read from. 
		using (StreamReader sr = File.OpenText(path)) 
		{
			string s = "";
			while ((s = sr.ReadLine()) != null) 
			{
				highscore = int.Parse(s);
			}
		}

			// This text is always added, making the file longer over time 
			// if it is not deleted. 
			using (StreamWriter sw = File.AppendText(path)) 
			{
				sw.WriteLine(highscore);
			}
	}
}
