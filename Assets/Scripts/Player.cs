﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Player : MonoBehaviour {

	public string name;
	public string nick;
	public int score;
	public List<GameObject> sheeps;

	public GameObject partidaGO;
	Partida partida;

	public GameObject canvas;
	public GameObject sheepPrefab;

	int nomeOvelha;
	public GameObject options;

	void Awake(){
		partida = partidaGO.GetComponent<Partida>();
		name = Settings.playerName;
		sheeps = new List<GameObject>();
		generateNick ();
	}

	void Start () {
		score = 0;
		nomeOvelha = 0;

		if (Settings.sheepsInit > Settings.sheepsMax)
			Settings.sheepsInit = Settings.sheepsMax;

		for(int i = 0; i < Settings.sheepsInit; i++){
			addOvelha();
		}
		partida.sheeps = sheeps;
	}
	
	// Update is called once per frame
	void Update () {
		//curralSheeps.text	= "x" + sheeps.Count;
	}

	public void removeElement(){
		Destroy (sheeps[0]);
		sheeps.RemoveAt (0);
	}

	public void generateNick(){
		if (name.Contains(" ")) {
			nick = name.Substring(0, name.IndexOf(" "));
		} else {
			nick = name;
		}
	}

	public string newSheepName(){
		string name;

		if (nomeOvelha < 10) {
			name = "Ovelha 0" + nomeOvelha;
		} else {
			name = "Ovelha " + nomeOvelha;
		}
		nomeOvelha++;
		return name;
	}

	public void addOvelha(){

		GameObject newSheep;
		/*newSheep = Instantiate(sheepPrefab, pos, Quaternion.identity) as GameObject;
		newSheep.transform.parent = canvas.transform;
		sheeps.Add (newSheep);*/

		Vector3 pos = new Vector3 (partida.ReferenceXCerca, partida.ReferenceYCerca, 0);
		int  tamanho			= sheeps.Count;
		bool isDiferent			= false;
		
		if(sheeps.Count == 0){
			newSheep = Instantiate(sheepPrefab, pos, Quaternion.identity) as GameObject;
			newSheep.GetComponent<Sheep> ().name = newSheepName();
			newSheep.GetComponent<Sheep> ().isCercado = true;
			newSheep.GetComponent<Sheep> ().options = options;
			newSheep.GetComponent<Sheep> ().playerGO = gameObject;
			newSheep.GetComponent<Sheep> ().partidaGO = partidaGO;
			newSheep.transform.parent = canvas.transform;
			sheeps.Add (newSheep);
		}else{
			int i = 0;
			while(true){
				//Debug.Log(pos.x + " - " + sheeps[i].transform.position.x + " | " + pos.y + "- " + sheeps[i].transform.position.y);
				if(pos.x == sheeps[i].transform.position.x && pos.y == sheeps[i].transform.position.y){
					isDiferent = false;
					pos.x += 30;
					if (pos.x > partida.ReferenceXCerca + 119) {
						pos.x = partida.ReferenceXCerca;
						pos.y -= 40;
					}
					i = 0;
				}
				++i;
				if(tamanho == i){
					isDiferent = true;
				}
				
				if(isDiferent){
					newSheep = Instantiate(sheepPrefab, pos, Quaternion.identity) as GameObject;
					newSheep.transform.parent = canvas.transform;
					newSheep.GetComponent<Sheep> ().name = newSheepName();
					newSheep.GetComponent<Sheep> ().isCercado = true;
					newSheep.GetComponent<Sheep> ().options = options;
					newSheep.GetComponent<Sheep> ().playerGO = gameObject;
					newSheep.GetComponent<Sheep> ().partidaGO = partidaGO;
					sheeps.Add (newSheep);
					break;
				}
				
			}
		}
	}

	public void putOnCercado(GameObject newSheep){
		
		Vector3 pos = new Vector3 (partida.ReferenceXCerca, partida.ReferenceYCerca, 0);
		int  tamanho			= sheeps.Count;
		bool isDiferent			= false;
		
		if(sheeps.Count == 0){
			newSheep.transform.position = pos;
			//sheeps.Add (newSheep);
		}else{
			int i = 0;
			while(true){
			//	Debug.Log(pos.x + " - " + sheeps[i].transform.position.x + " | " + pos.y + "- " + sheeps[i].transform.position.y);
				if(pos.x == sheeps[i].transform.position.x && pos.y == sheeps[i].transform.position.y){
					isDiferent = false;
					pos.x += 30;
					if (pos.x > partida.ReferenceXCerca + 119) {
						pos.x = partida.ReferenceXCerca;
						pos.y -= 40;
					}
					i = 0;
				}else{
					i++;
					if(tamanho == i){
						isDiferent = true;
					}
					
					if(isDiferent){
						newSheep.transform.position = pos;
					//	sheeps.Add (newSheep);
						break;
					}
				}
			}
		}
	}

	public void buysheep(){
		if((sheeps.Count) < 21){
			if(score >= (Settings.comprarScore * -1)){
				string msg = Analytics.dataAtual + "[COMPRA] - Ovelha " + nomeOvelha;
				msg += " - Grama: " + partida.grass + " - Pontos do jogador: " + score;
				Analytics.addToLog (msg);
				addOvelha();
				score += Settings.comprarScore;
			}
		}

	}

}
