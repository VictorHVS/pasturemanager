﻿using UnityEngine;
using System.Collections;


public class Settings : MonoBehaviour {

	public static bool start = true;

	public static int sheepsInit; //Quantidade inicial de ovelhas
	public static int sheepsMax; //Quantidade maxima de ovelhas
	public static int sheepStavation; //Quantas rodadas uma ovelha sobrevive com fome
	//public static int sheepLifeDuration; //Espectativa de vida da ovelha
	public static int sheepBitPerRound; //Quanto uma ovelha come por rodada
	public static int roundDuration; //Duracao de uma rodada (s)
	public static int roundQuant; //Quantidade de Rodadas
	public static int grassReajust; //Taxa de reajuste da grama por rodada
	public static int grassSingleQuant; //Quantidade Inicial de grama no modo single
	public static int grassGroupQuant; //Quantidade Inicial de grama no modo multplayer
	public static int grassCostMaintenance; //Custo de manutençao do pasto

	//Acoes
	//O Jogador so recebe a pontuacao ao fim das acoes
	public static int cercadoDuration; 
	public static int cercadoScore; 
	public static int pastoDuration; 
	public static int pastoScore; 
	public static int tosarDuration; 
	public static int tosarScore; 
	public static int reproduzirDuration; //Ganha uma ovelha nova ao fim
	public static int venderDuration; 
	public static int venderScore; 
	public static int comprarDuration; //Ganha uma ovelha nova ao fim
	public static int comprarScore;
	public static int roubarDuration; //Ganha uma ovelha usada ao fim

	//Players
	public static string playerName;

	//Type of test

	void Start(){
		if (start) {
			init ();
			start = false;
		}
	}

	void init(){
		sheepsInit 	= 5;
		sheepsMax 	= 20;
		sheepStavation = 5;
		//sheepLifeDuration = 10;
		sheepBitPerRound = 10;
		roundDuration = 15;
		roundQuant = 110;
		grassReajust = 10;
		grassSingleQuant = 200;
		grassGroupQuant = 400;
		grassCostMaintenance = 30;

		cercadoDuration = 0;
		cercadoScore = 0;
		pastoDuration = 2;
		pastoScore = 5;
		tosarDuration = 4;
		tosarScore = 10;
		reproduzirDuration = 6;
		venderDuration = 0;
		venderScore = 20;
		comprarDuration = 0;
		comprarScore = -40;
		roubarDuration = 1;

	}
}