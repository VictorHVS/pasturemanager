﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class Partida : MonoBehaviour {

	public GameObject player;
	Player playerScript;

	public List<GameObject> sheeps;
	public List<GameObject> sheepsPasture;
	public List<GameObject> sheepsTransoes;
	
	public int ReferenceXCerca;
	public int ReferenceYCerca;
	public int ReferenceXPasto;
	public int ReferenceYPasto;
	public int ReferenceXTosa;
	public int ReferenceYTosa;
	public int ReferenceXReproducao;
	public int ReferenceYReproducao;

	public int round;
	public int grass;
	public int rodadaTemp;

	public int quantOvelhas;

	public Text quantGrass;

	/*int min;
	int sec;
	int fraction;*/
	public float timecount;
	float starttime;

	void Start ()
	{
		sheeps = new List<GameObject>();
		sheepsTransoes = new List<GameObject> ();
		sheepsPasture = new List<GameObject> ();
		playerScript = player.GetComponent<Player> ();
		grass 		= Settings.grassSingleQuant;
		round 		= 0;
		starttime 	= Time.time;
		rodadaTemp 	= 0;
		attGrass ();
	}
	
	void Update () {
		timecount = Time.time - starttime;
		round = (int)(timecount / Settings.roundDuration);
		germinatorGrass (round);
		quantOvelhas = playerScript.sheeps.Count;
	}

	public void attGrass(){
		if (grass < 10) {
			quantGrass.text = "x00" + grass;
		} else if (grass < 100) {
			quantGrass.text = "x0" + grass;
		}else if(grass < 0) {
			quantGrass.text = "\\o/";
		}else {
			quantGrass.text = "x" + grass;
		}
	}

	public void germinatorGrass(int round){
		cafetao();
		if (rodadaTemp < round){
			grass = (int) (grass * ((float)Settings.grassReajust+100)/100);
			attGrass();
			rodadaTemp++;
			checkGameOver ();
		}
	}

	public void cafetao(){
		for (int i = 0; i < sheepsTransoes.Count; i+=2) {
			sheepsTransoes[i].GetComponent<Sheep>().parceiroSexual = sheepsTransoes[i+1];
			sheepsTransoes[i+1].GetComponent<Sheep>().parceiroSexual = sheepsTransoes[i];

			if(sheepsTransoes[i].GetComponent<Sheep> ().busy == 0 && sheepsTransoes[i].GetComponent<Sheep> ().isReproduzindo){
				player.GetComponent<Player> ().addOvelha();
				string msg = Analytics.dataAtual + "[NASCIMENTO] - Ovelha " + player.GetComponent<Player> ().sheeps[player.GetComponent<Player> ().sheeps.Count-1].name;
				msg += " - Grama: " + grass + " - Pontos do jogador: " + playerScript.score;
				Analytics.addToLog (msg);

				sheepsTransoes[i].GetComponent<Sheep> ().isReproduzindo = false;
				sheepsTransoes[i+1].GetComponent<Sheep> ().isReproduzindo = false;

				sheepsTransoes.RemoveAt(i);
				sheepsTransoes.RemoveAt(i);
			}
		}

	}

	public void repeatReproducao(GameObject newSheep){
		Sheep sheepScript = newSheep.GetComponent<Sheep> ();
		for (int i = 0; i < 2; i++) {
			Debug.Log(sheepScript.name + "Coisando d novo");

				sheepScript.isReproduzindo = true;
				sheepScript.isReproduzir = true;		
				sheepScript.sacrifice = true;

				sheepScript.hunger = Settings.sheepStavation + 3;

				sheepScript.changeStatus ("isReproduzir");
				sheepScript.busy = Settings.reproduzirDuration;
				sheepScript.tempToNextAction = Settings.reproduzirDuration + 1;
				sheepsTransoes.Add (newSheep);	

			sheepScript = newSheep.GetComponent<Sheep>().parceiroSexual.GetComponent<Sheep>();
		}
	}

	public bool reproduzir(GameObject newSheep){
		List<GameObject> pombinhos = new List<GameObject>();
		pombinhos.Add(newSheep);
		bool formouPar = false;
		newSheep.GetComponent<Sheep>().hunger = -Settings.sheepStavation;
		newSheep.GetComponent<Sheep>().tempToNextAction = Settings.reproduzirDuration+1;
		for(int j = 0; j < sheeps.Count; j++){
			if(sheeps[j].GetComponent<Sheep>().busy == 0 && !newSheep.Equals(sheeps[j])){
				sheeps[j].GetComponent<Sheep>().hunger =-Settings.sheepStavation;
				pombinhos.Add(sheeps[j]);
			}
			if(pombinhos.Count == 2){
				formouPar = true;
				break;
			}
		}

		if (formouPar) {
			Vector3 pos = new Vector3 (ReferenceXReproducao, ReferenceYReproducao, 0);
			int  tamanho			= sheeps.Count;
			bool isDiferent			= false;
			int i = 0;

			while(pombinhos.Count != 0){	
				if(pos.x == sheeps[i].transform.position.x && pos.y == sheeps[i].transform.position.y){				
					isDiferent = false;
					pos.x += 50;
					if (pos.x > 1324) {
						pos.x = ReferenceXReproducao;
						pos.y -= 50;
					}
					i = 0;
				}else{
					i++;
					if(tamanho == i){
						isDiferent = true;
					}
					
					if(isDiferent){
						string msg = Analytics.dataAtual + "[REPRODUCAO] - Ovelha " + pombinhos[0].GetComponent<Sheep> ().name;
						msg += " - Grama: " + grass + " - Pontos do jogador: " + playerScript.score;
						Analytics.addToLog (msg);
						pombinhos[0].transform.position = pos;
						Sheep sheepScript = pombinhos[0].GetComponent<Sheep> ();
						sheepScript.isReproduzindo = true;
						sheepScript.isReproduzir = true;

						//sheepScript.sacrifice = true;

						sheepScript.changeStatus("isReproduzir");
						sheepScript.busy = Settings.reproduzirDuration;
						sheepScript.tempToNextAction = Settings.reproduzirDuration+1;
						sheepsTransoes.Add(pombinhos[0]);
						pombinhos.RemoveAt(0);
						pos = new Vector3 (ReferenceXReproducao, ReferenceYReproducao, 0);
						tamanho			= sheeps.Count;
						isDiferent			= false;
						i = 0;
					}
				}
			} //fim while
		} //fim if
		return formouPar;
	}

	public void checkGameOver(){
		if(round == Settings.roundQuant || grass <= 0 || (playerScript.sheeps.Count) <= 0 && sheeps.Count <= 0){
			Application.LoadLevel("End Game");
		}
	}

	public void addOnPastureAndCheckAll(GameObject newSheep){
		Vector3 pos = new Vector3 (ReferenceXPasto, ReferenceYPasto, 0);
		newSheep.transform.position = pos;
		sheepsPasture.Add (newSheep);

		for (int i =0; i < sheepsPasture.Count; i++) {
			pos = sheepsPasture[i].transform.position;
			for (int j =0; j < sheepsPasture.Count; j++) {
				if(sheepsPasture[i].transform.position.x == sheepsPasture[j].transform.position.x){
				//	if(pos.x > )
					if(sheepsPasture[i].transform.position.y == sheepsPasture[j].transform.position.y){
						pos.x += 50;
						sheepsPasture[i].transform.position = pos;
					}	else if (sheepsPasture[i].transform.position.y > -260){
						pos.y -= 50;
						sheepsPasture[i].transform.position = pos;
					}
				}
			}
		}

	}

	public void putOnPasture(GameObject newSheep){
				//	addOnPastureAndCheckAll (newSheep);

				Vector3 pos = new Vector3 (ReferenceXPasto, ReferenceYPasto, 0);
				int tamanho = sheeps.Count;
				bool isDiferent = false;
		
				if (sheeps.Count == 0) {
						Debug.Log (newSheep.transform.position.x.ToString ());
						newSheep.transform.position = pos;
						//sheeps.Add (newSheep);
				} else {
						int i = 0;
						while (true) {	
								if (pos.x == sheeps [i].transform.position.x && pos.y == sheeps [i].transform.position.y) {				
										isDiferent = false;
										pos.x += 50;
										if (pos.x > 425) {
												pos.x = ReferenceXPasto;
												pos.y -= 50;
										}
										i = 0;
								} else {
										i++;
										if (tamanho == i) {
												isDiferent = true;
										}
					
										if (isDiferent) {
												newSheep.transform.position = pos;
												//	sheeps.Add (newSheep);
												break;
										}
								}
						}
				}
		}

	public void putOnTosa(GameObject newSheep){
		
		Vector3 pos = new Vector3 (ReferenceXTosa, ReferenceYTosa, 0);
		int  tamanho			= sheeps.Count;
		bool isDiferent			= false;
		
		if(sheeps.Count == 0){
			Debug.Log(newSheep.transform.position.x.ToString());
			newSheep.transform.position = pos;
			//sheeps.Add (newSheep);
		}else{
			int i = 0;
			while(true){	
				if(pos.x == sheeps[i].transform.position.x && pos.y == sheeps[i].transform.position.y){				
					isDiferent = false;
					pos.x += 50;
					if (pos.x > 860) {
						pos.x = ReferenceXTosa;
						pos.y -= 50;
					}
					i = 0;
				}else{
					i++;
					if(tamanho == i){
						isDiferent = true;
					}
					
					if(isDiferent){
						newSheep.transform.position = pos;
					//	sheeps.Add (newSheep);
						break;
					}
				}
			}
		}
	}

}