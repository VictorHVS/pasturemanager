﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SheepActions : MonoBehaviour {

	public GameObject partidaGO;
	Partida partida;
	public GameObject playerGO;
	Player player;

	public GameObject sheepGO;
	public Sheep sheep;
	public Text title;

	public GameObject buttonCercado;
	public GameObject buttonPasture;
	public GameObject buttonTosar;
	public GameObject buttonVender;
	public GameObject buttonReproduzir;

	// Use this for initialization
	void Start () {
		player = playerGO.GetComponent<Player>();
		partida = partidaGO.GetComponent<Partida>();
	}
	
	// Update is called once per frame
	void Update () {
		//texto.text = Player.sheeps.Count + "";
	}

	public void attStatus(){
		gameObject.SetActive(true);
		title.text = "Açoes para a " + sheepGO.GetComponent<Sheep>().name;
		if (sheep.busy != 0) {
			buttonPasture.SetActive (false);
			buttonCercado.SetActive (false);
			buttonTosar.SetActive (false);
			buttonVender.SetActive (false);
			buttonReproduzir.SetActive (false);
		}else{
			buttonPasture.SetActive (true);
			buttonCercado.SetActive (true);
			buttonTosar.SetActive (true);
			buttonVender.SetActive (true);
			buttonReproduzir.SetActive (true);
		}/* else {
			if (sheep.isPasture) {
				buttonPasture.SetActive (false);
			} else {
				buttonPasture.SetActive (true);
			}
			
			if(sheep.isCercado){
				buttonCercado.SetActive(false);
			} else {
				buttonCercado.SetActive (true);
			}
			
			if(sheep.isTosa){
				buttonTosar.SetActive(false);
			} else {
				buttonTosar.SetActive (true);
			}
			
			if(sheep.isVendivel){
				buttonVender.SetActive(false);
			} else {
				buttonVender.SetActive (true);
			}

			if(sheep.isReproduzir){
				buttonReproduzir.SetActive(false);
			} else {
				buttonReproduzir.SetActive (true);
			}

		}*/
	}

	public void close(){
		gameObject.SetActive (false);
	}

	//isso eh feio... mas tou com pressa.


	public void putOnPasture(){
		string msg = Analytics.dataAtual + "[PASTO] - Ovelha " + sheep.name;
		msg += " - Grama: " + partida.grass + " - Pontos do jogador: " + player.score;
		Analytics.addToLog (msg);
		sheep.changeStatus ("isPasture");
		sheep.tempToNextAction = Settings.pastoDuration+1;
		sheep.busy = Settings.pastoDuration;
		sheep.pontuacao (Settings.pastoDuration, Settings.pastoScore);
		partida.putOnPasture (sheepGO);
		close ();
	}

	public void putOnCercado(){
		string msg = Analytics.dataAtual + "[CERCADO] - Ovelha " + sheep.name;
		msg += " - Grama: " + partida.grass + " - Pontos do jogador: " + player.score;
		Analytics.addToLog (msg);
		sheep.changeStatus ("isCercado");
		sheep.tempToNextAction = Settings.cercadoDuration +1;
		player.putOnCercado (sheepGO);
		close ();
	}

	public void tosar(){
		if (!sheep.lastAction.Equals ("isReproduzir")) {
			if (sheep.lastAction.Equals ("isTosa")) {
					sacrifice ("isTosa");
			} else {
					string msg = Analytics.dataAtual + "[TOSA] - Ovelha " + sheep.name;
					msg += " - Grama: " + partida.grass + " - Pontos do jogador: " + player.score;
					Analytics.addToLog (msg);
					sheep.changeStatus ("isTosa");
					sheep.busy = Settings.tosarDuration;
					sheep.tempToNextAction = Settings.tosarDuration + 1;
					sheep.hunger = -Settings.sheepStavation;
					sheep.pontuacao (Settings.tosarDuration, Settings.tosarScore);
					partida.putOnTosa (sheepGO);
					close ();
			}
		}
	}

	public void vender(){
		string msg = Analytics.dataAtual + "[VENDA] - Ovelha " + sheep.name;
		msg += " - Grama: " + partida.grass + " - Pontos do jogador: " + player.score;
		Analytics.addToLog (msg);
		player.score += Settings.venderScore;
		partida.sheeps.Remove (sheepGO);
		Destroy (sheepGO);
		close ();
	}

	public void sacrifice(string tipo){
		if (tipo.Equals ("isTosa")) {
			string msg = Analytics.dataAtual + "[SACRIFICIO POR TOSA] - Ovelha " + sheep.name;
			msg += " - Grama: " + partida.grass + " - Pontos do jogador: " + player.score;
			Analytics.addToLog (msg);
			sheep.sacrifice = true;
			sheep.changeStatus ("isTosa");
			sheep.busy = Settings.tosarDuration;	
			sheep.hunger = -Settings.sheepStavation;
			sheep.pontuacao (Settings.tosarDuration, Settings.tosarScore);
			close ();
		} else if (tipo.Equals ("isReproduzir")) {
			partida.repeatReproducao(sheepGO);
			close ();
		}
}

	public void reproduzir(){
		if (sheep.lastAction.Equals ("isReproduzir")) {
			Debug.Log("SACRIFICIO POR REPRODUCAO");
			string msg;
			msg = Analytics.dataAtual + "[SACRIFICIO POR REPRODUCAO] - Ovelha " + name;
			msg += " - Grama: " + partida.grass + " - Pontos do jogador: " + player.score;
			msg = Analytics.dataAtual + "[SACRIFICIO POR REPRODUCAO] - Ovelha " + sheep.GetComponent<Sheep>().parceiroSexual.GetComponent<Sheep>().name;
			msg += " - Grama: " + partida.grass + " - Pontos do jogador: " + player.score;
			Analytics.addToLog (msg);
			sacrifice("isReproduzir");
		} else {
			if (sheep.isReproduzir) {
				player.addOvelha();				
			}
			partida.reproduzir (sheepGO);
			close ();
		}
	}

}