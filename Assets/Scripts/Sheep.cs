﻿using UnityEngine;
using System.Collections;

public class Sheep : MonoBehaviour {

	public GameObject partidaGO;
	Partida partida;
	public GameObject playerGO;
	Player player;

	public GameObject parceiroSexual;

//	public	 int 		life;
	public string 	name;
	public int 		hunger;

	public int		busy;

	public bool isCercado;
	public bool isPasture;
	public bool isTosa;
	public bool isVendivel;
	public bool isReproduzir;
	public bool isReproduzindo;

	public bool sacrifice;

	public string lastAction;
	public int tempToNextAction;

	public GameObject options;
	public GameObject alert;

	public int rodadaTemp;
	public int scoreTemp;

	// Use this for initialization
	void Start () {
		player = playerGO.GetComponent<Player>();
		partida = partidaGO.GetComponent<Partida>();
		//life = Settings.sheepLifeDuration;
		busy = 0;
		hunger = 0;
		rodadaTemp 	= partida.rodadaTemp;
		tempToNextAction = -1;
		sacrifice = false;
	}
	
	// Update is called once per frame
	void Update () {
		attStatus ();
		checkSheep ();
	}

	public void showOptions(){
		options.GetComponent<SheepActions> ().sheepGO = gameObject;
		options.GetComponent<SheepActions> ().sheep = gameObject.GetComponent<Sheep>();
		options.GetComponent<SheepActions> ().attStatus ();
		options.SetActive (true);
	}

	public void pontuacao(int duracao,int score){
		busy = duracao;
		scoreTemp = score;
	}

	public void attStatus(){
		if(busy > 0)
			alert.SetActive(false);
		else
			alert.SetActive(true);
		if (rodadaTemp < partida.round){
		//	life--;
			hunger++;
			if(busy > 0){
				alert.SetActive(false);
				busy--;
				if(isTosa || isReproduzindo)
					hunger = Settings.sheepStavation-1;
			}
			if(tempToNextAction > 0){
				tempToNextAction--;
			}
			if(busy == 0){
				if(this.isPasture){
					Debug.Log("Vai comer grama....");
					partida.grass -= Settings.sheepBitPerRound;
					partida.attGrass();
				}	
				player.score += scoreTemp;
				scoreTemp = 0;
				if(sacrifice){
					partida.cafetao();
					options.SetActive (false);
					if(player.sheeps.Contains(gameObject)){
						string msg = Analytics.dataAtual + "[MORTE - SACRIFICIO] - Ovelha " + name;
						msg += " - Grama: " + partida.grass + " - Pontos do jogador: " + player.score;
						Analytics.addToLog (msg);
						Destroy (gameObject);
						player.sheeps.Remove(gameObject);
					}else{
						Destroy (gameObject);
						partida.sheeps.Remove(gameObject);
					}
				}
				repeatAction();
			}
			rodadaTemp++;
		}
		if(isPasture){
			hunger = 0;
		}
	}

	public void repeatAction(){
		string msg;
		if (tempToNextAction==0) {
			switch (lastAction)
			{			
			case "isTosa":
				Debug.Log("SACRIFICIO POR TOSA");
				msg = Analytics.dataAtual + "[SACRIFICIO POR TOSA] - Ovelha " + name;
				msg += " - Grama: " + partida.grass + " - Pontos do jogador: " + player.score;
				Analytics.addToLog (msg);
				sacrifice = true;
				changeStatus ("isTosa");
				busy = Settings.tosarDuration;	
				hunger = -Settings.sheepStavation;
				pontuacao (Settings.tosarDuration, Settings.tosarScore);
				break;
			case "isPasture":
				msg = Analytics.dataAtual + "[REPETINDO PASTO] - Ovelha " + name;
				msg += " - Grama: " + partida.grass + " - Pontos do jogador: " + player.score;
				Analytics.addToLog (msg);
				changeStatus ("isPasture");
				tempToNextAction = Settings.pastoDuration+1;
				busy = Settings.pastoDuration;
				pontuacao (Settings.pastoDuration, Settings.pastoScore);
				options.SetActive (false);
			//	partida.putOnPasture();
				break;
			case "isReproduzir":
				if(isReproduzir && parceiroSexual.GetComponent<Sheep>().isReproduzir){
				Debug.Log("SACRIFICIO POR REPRODUCAO");
				msg = Analytics.dataAtual + "[SACRIFICIO POR REPRODUCAO] - Ovelha " + name;
				msg += " - Grama: " + partida.grass + " - Pontos do jogador: " + player.score;
				msg = Analytics.dataAtual + "[SACRIFICIO POR REPRODUCAO] - Ovelha " + parceiroSexual.GetComponent<Sheep>().name;
				msg += " - Grama: " + partida.grass + " - Pontos do jogador: " + player.score;
				Analytics.addToLog (msg);
				Debug.Log(gameObject.GetComponent<Sheep>().name);
				Debug.Log(parceiroSexual.GetComponent<Sheep>().name);

				partida.repeatReproducao(gameObject);
				}
				break;
			}
		}
	}

	public void changeStatus(string status){
		lastAction = status;
		switch (status)
		{
		case "isCercado":
			isCercado = true;
			isPasture = false;
			isReproduzir = false;
			isTosa = false;
			isVendivel = false;
			break;
		case "isPasture":
			isCercado = false;
			isPasture = true;
			isReproduzir = false;
			isTosa = false;
			isVendivel = false;
			break;
		case "isReproduzir":
			isCercado = false;
			isPasture = false;
			isReproduzir = true;
			isTosa = false;
			isVendivel = false;
			break;
		case "isTosa":
			isCercado = false;
			isPasture = false;
			isReproduzir = false;
			isTosa = true;
			isVendivel = false;
			break;
		}
	}

	public void checkSheep(){
		if(hunger == Settings.sheepStavation){
			options.SetActive (false);
			if(player.sheeps.Contains(gameObject)){
				string msg = Analytics.dataAtual + "[MORTE] - Ovelha " + name;
				msg += " - Grama: " + partida.grass + " - Pontos do jogador: " + player.score;
				Analytics.addToLog (msg);
				Destroy (gameObject);
				player.sheeps.Remove(gameObject);
			}else{
				Destroy (gameObject);
				partida.sheeps.Remove(gameObject);
			}
		}
	}

}
