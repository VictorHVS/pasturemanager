﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

public class EndGameInterfaceAdapter : MonoBehaviour {

	public Text title;
	public Text score;

	public void Start(){
		title.text = "Parabens, " + Analytics.player.nick + ".";
		score.text = ""+Analytics.player.score;

		String msg = "[Pontuaçao Final: " + Analytics.player.score + "]"; 
		msg += "[Fim de jogo] - "+Analytics.dataAtual  + Environment.NewLine;
		msg += "=============== Configuraçoes ================" + Environment.NewLine;
		msg += "Ovelhas Iniciais: " + Settings.sheepsInit + Environment.NewLine;
		msg += "Nº Max de Ovelhas: " + Settings.sheepsMax + Environment.NewLine;
		msg += "Rodadas sem comer: " + Settings.sheepStavation + Environment.NewLine;
		msg += "Unidades consumidas de grama: " + Settings.sheepBitPerRound + Environment.NewLine;
		msg += "Duraçao da rodada: " + Settings.roundDuration + Environment.NewLine;
		msg += "Quantidade de Rodadas: " + Settings.roundQuant + Environment.NewLine;
		msg += "Reajuste da grama em %: " + Settings.grassReajust + Environment.NewLine;
		msg += "Quant de grama inicial: " + Settings.grassSingleQuant + Environment.NewLine;
		msg += Environment.NewLine;
		msg += "Duraçao no cercado em rodadas: " + Settings.cercadoDuration + Environment.NewLine;		
		msg += "Duraçao no pasto em rodadas: " + Settings.pastoDuration + Environment.NewLine;		
		msg += "Pontos do pasto: " + Settings.pastoScore + Environment.NewLine;
		msg += "Duraçao na tosa em rodadas: " + Settings.tosarDuration + Environment.NewLine;		
		msg += "Pontos da tosa: " + Settings.tosarScore + Environment.NewLine;
		msg += "Duraçao na reproduçao em rodadas: " + Settings.reproduzirDuration + Environment.NewLine;		
		msg += "Bonificaçao da fornicaçao: Uma ovelha 0Km"+ Environment.NewLine;
		msg += "Duraçao da venda em rodadas: " + Settings.venderDuration + Environment.NewLine;		
		msg += "Pontos da venda: " + Settings.venderScore + Environment.NewLine;
		msg += "Duraçao da compra em rodadas: " + Settings.comprarDuration + Environment.NewLine;		
		msg += "Pontos da compra: " + Settings.comprarScore + Environment.NewLine;
		msg += "=============== Configuraçoes ================";
		Analytics.addToLog (msg);
	}
}