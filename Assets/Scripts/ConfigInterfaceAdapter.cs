using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ConfigInterfaceAdapter : MonoBehaviour {

	public InputField sheepsInit; //Quantidade inicial de ovelhas
	public InputField sheepsMax; //Quantidade maxima de ovelhas
	public InputField sheepStavation; //Quantas rodadas uma ovelha sobrevive com fome
	public InputField sheepLifeDuration; //Espectativa de vida da ovelha
	public InputField sheepBitPerRound; //Quanto uma ovelha come por rodada
	public InputField roundDuration; //Duracao de uma rodada (s)
	public InputField roundQuant; //Quantidade de Rodadas
	public InputField grassReajust; //Taxa de reajuste da grama por rodada
	public InputField grassSingleQuant; //Quantidade Inicial de grama no modo single
	public InputField grassGroupQuant; //Quantidade Inicial de grama no modo multplayer
	public InputField grassCostMaintenance; //Custo de manutençao do pasto

	public InputField cercadoDuration;
	public InputField cercadoScore;
	public InputField pastoDuration;
	public InputField pastoScore;
	public InputField tosarDuration;
	public InputField tosarScore;
	public InputField reproduzirDuration;
	public InputField venderDuration;
	public InputField venderScore;
	public InputField comprarDuration;
	public InputField comprarScore;
	public InputField roubarDuration;

	void Start(){
		attElements ();
	}
	
	public void save(){
		Settings.sheepsInit = int.Parse(sheepsInit.text);
		Settings.sheepsMax 	= int.Parse(sheepsMax.text);
		Settings.sheepStavation = int.Parse(sheepStavation.text);
//		Settings.sheepLifeDuration = int.Parse(sheepLifeDuration.text);
		Settings.sheepBitPerRound = int.Parse(sheepBitPerRound.text);
		Settings.roundDuration = int.Parse(roundDuration.text);
		Settings.roundQuant = int.Parse(roundQuant.text);
		Settings.grassReajust = int.Parse(grassReajust.text);
		Settings.grassSingleQuant = int.Parse(grassSingleQuant.text);
		Settings.grassGroupQuant = int.Parse(grassGroupQuant.text);
		Settings.grassCostMaintenance = int.Parse(grassCostMaintenance.text);
		
		Settings.cercadoDuration = int.Parse(cercadoDuration.text);
		Settings.cercadoScore = int.Parse(cercadoScore.text);
		Settings.pastoDuration = int.Parse(pastoDuration.text);
		Settings.pastoScore = int.Parse(pastoScore.text);
		Settings.tosarDuration = int.Parse(tosarDuration.text);
		Settings.tosarScore = int.Parse(tosarScore.text);
		Settings.reproduzirDuration = int.Parse(reproduzirDuration.text);
		Settings.venderDuration = int.Parse(venderDuration.text);
		Settings.venderScore = int.Parse(venderScore.text);
		Settings.comprarDuration = int.Parse(comprarDuration.text);
		Settings.comprarScore = int.Parse(comprarScore.text);
		Settings.roubarDuration = int.Parse(roubarDuration.text);

		Application.LoadLevel("Menu Principal");
		
	}

	public void attElements(){
		sheepsInit.text = Settings.sheepsInit + "";
		sheepsMax.text = Settings.sheepsMax + "";
		sheepStavation.text = Settings.sheepStavation + "";
//		sheepLifeDuration.text = Settings.sheepLifeDuration + "";
		sheepBitPerRound.text = Settings.sheepBitPerRound + "";
		roundDuration.text = Settings.roundDuration + "";
		roundQuant.text = Settings.roundQuant + "";
		grassReajust.text = Settings.grassReajust + "";
		grassSingleQuant.text = Settings.grassSingleQuant + "";
		grassGroupQuant.text = Settings.grassGroupQuant + "";
		grassCostMaintenance.text = Settings.grassCostMaintenance + "";

		cercadoDuration.text = Settings.cercadoDuration + "";
		cercadoScore.text = Settings.cercadoScore + "";
		pastoDuration.text = Settings.pastoDuration + "";
		pastoScore.text = Settings.pastoScore + "";
		tosarDuration.text = Settings.tosarDuration + "";
		tosarScore.text = Settings.tosarScore + "";
		reproduzirDuration.text = Settings.reproduzirDuration + "";
		venderDuration.text = Settings.venderDuration + "";
		venderScore.text = Settings.venderScore + "";
		comprarDuration.text = Settings.comprarDuration + "";
		comprarScore.text = Settings.comprarScore + "";
		roubarDuration.text = Settings.roubarDuration + "";
	}

}