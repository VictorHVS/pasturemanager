﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ButtonsScript : MonoBehaviour {
	
//	public InputField nome;

	// Use this for initialization
	void Start () {
	
	}

	public void siteBracode(){
		Application.OpenURL("http://bracode.com.br/");
	}

	public void sair(){
		Application.Quit();
	}

	public void loadMenu(){
		Application.LoadLevel("Menu Principal");
	}

	public void loadCreditos(){
		Application.LoadLevel("Creditos");
	}

	public void loadConfig(){
		Application.LoadLevel("Configuracoes");
	}

	public void loadExperimentos(){
		Application.LoadLevel("Menu Experimento");
	}

	public void loadCadastro(){
		Application.LoadLevel("Cadastro");
	}

	public void loadJogo(){
		Application.LoadLevel("Jogo");
	}

	public void loadFase(){
		Application.LoadLevel("Menu Fase");
	}
	
}
